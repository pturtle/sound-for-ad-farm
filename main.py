import requests
import time
from pydub import AudioSegment
from pydub.playback import play
from config import CONFIG

def sound():
	song = AudioSegment.from_ogg('sound/sound.ogg')
	song += 5
	play(song)

with open('count.txt', 'r') as f:
	count=int(f.read())

while True:
	try:
		r=requests.get('http://{}:{}/{}'.format(CONFIG['IP'], CONFIG['PORT'], CONFIG['FILE']))
		print(count, int(r.text))
		for i in range(count, int(r.text)):
			sound()
			time.sleep(0.3)
		count=int(r.text)
		with open('count.txt', 'w+') as f:
			f.write(str(count))
		time.sleep(1)
	except Exception as ex:
		print(ex)
		time.sleep(1)
	
	
